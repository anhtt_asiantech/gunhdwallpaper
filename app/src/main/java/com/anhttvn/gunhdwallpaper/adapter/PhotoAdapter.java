package com.anhttvn.gunhdwallpaper.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.gunhdwallpaper.R;
import com.anhttvn.gunhdwallpaper.model.Wallpaper;
import com.squareup.picasso.Picasso;
import java.util.List;


public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.ViewHolderWallpaper> implements View.OnClickListener {
  private Context mContext;
  private List<Wallpaper> listImage;
  private OnclickImage mOnclick;
  private boolean mConnect;
  public PhotoAdapter(Context context, List<Wallpaper> list, boolean connect, OnclickImage click) {
    mContext = context;
    listImage = list;
    mOnclick = click;
    mConnect = connect;
  }
  @NonNull
  @Override
  public ViewHolderWallpaper onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mContext)
            .inflate(R.layout.item_home_wallpaper,parent,false);
    ViewHolderWallpaper itemView = new ViewHolderWallpaper(view);
    return itemView;
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolderWallpaper holder, int position) {
    Wallpaper wallpaper = listImage.get(position);
    if (wallpaper == null) {
      return;
    }
    if (mConnect) {
      holder.warming.setVisibility(View.GONE);
    }
    holder.countView.setText(String.valueOf(wallpaper.getView()));
    holder.imgFavorite.setImageResource(
            wallpaper.getFavorite() == 1 ? R.drawable.ic_favorite_disabel : R.drawable.ic_favorite_enable);
    if (wallpaper.getPath() == null || wallpaper.getPath().isEmpty()) {
      holder.item.setImageResource(R.drawable.ic_no_thumbnail);
    } else {
      Picasso.with(mContext).load(wallpaper.getPath())
              .placeholder(R.drawable.ic_no_thumbnail)
              .error(R.drawable.ic_no_thumbnail)
              .into(holder.item);
    }
    holder.item.setTag(position);
    holder.item.setOnClickListener(this);
  }

  @Override
  public int getItemCount() {
    return listImage.size();
  }

  @Override
  public void onClick(View v) {
    int position = Integer.parseInt(v.getTag()+"");
    switch (v.getId()){
      case R.id.item:
        mOnclick.selectedPosition(position);
        break;

    }
  }


  public class ViewHolderWallpaper extends RecyclerView.ViewHolder {
    private ImageView item;
    private TextView countView;
    private ImageView imgFavorite;
    private TextView warming;
    public ViewHolderWallpaper(View view) {
      super(view);
      item = view.findViewById(R.id.item);
      countView = view.findViewById(R.id.countView);
      imgFavorite = view.findViewById(R.id.imgFavorite);
      warming = view.findViewById(R.id.noInternet);
    }
  }

  public interface OnclickImage {
    void selectedPosition(int position);
  }
}
