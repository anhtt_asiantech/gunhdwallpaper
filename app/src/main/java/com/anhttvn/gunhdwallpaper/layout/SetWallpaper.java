package com.anhttvn.gunhdwallpaper.layout;

import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.anhttvn.gunhdwallpaper.R;
import com.anhttvn.gunhdwallpaper.databinding.ActivitySetWallpaperBinding;
import com.anhttvn.gunhdwallpaper.model.MessageEvent;
import com.anhttvn.gunhdwallpaper.model.Wallpaper;
import com.anhttvn.gunhdwallpaper.util.BaseActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;

/**
 * @author anhtt61
 * @version 1.1.1
 * @2022
 */
public class SetWallpaper extends BaseActivity {
  private ActivitySetWallpaperBinding setWallpaperBinding;
  private Wallpaper wallpaper;
  private boolean isAllVisibleFab = false;

  @Override
  public void init() {
    getSupportActionBar().hide();
    setWallpaperBinding.fab.homeWall.setVisibility(View.GONE);
    setWallpaperBinding.fab.lockWall.setVisibility(View.GONE);
    setWallpaperBinding.fab.favorite.setVisibility(View.GONE);
    setWallpaperBinding.fab.download.setVisibility(View.GONE);
    setWallpaperBinding.fab.bottom.setVisibility(View.GONE);

    isBannerADS(setWallpaperBinding.ads);
    getDataIntent();
    eventSetWallpaper();
    changeIconFavorite(db.isFavoriteWallpaper(wallpaper.getId()));
  }


  private void eventSetWallpaper() {
    setWallpaperBinding.fab.menu.setOnClickListener(v -> {
      isMenuDisable();
    });

    setWallpaperBinding.fab.homeWall.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      setWallpaper("Home");
    });
    setWallpaperBinding.fab.lockWall.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      setWallpaper("Lock");
    });

    setWallpaperBinding.fab.download.setOnClickListener(v -> {
      if (wallpaper == null || wallpaper.getPath() == null) {
        return;
      }
      downloadImage(wallpaper.getPath(), wallpaper.getTitle());
    });

    setWallpaperBinding.fab.favorite.setOnClickListener(v -> {
      changeFavorite();
    });
    setWallpaperBinding.fab.bottom.setOnClickListener(v -> {
      EventBus.getDefault().postSticky(new MessageEvent("UPDATE"));
      isADSFull();
      finish();
    });
  }

  private void changeIconFavorite(boolean is) {
    setWallpaperBinding.fab.favorite.setImageResource(is ? R.drawable.ic_favorite_disabel : R.drawable.icon_favorite_disable);
  }

  private void changeFavorite() {
    boolean isExits = db.isFavoriteWallpaper(wallpaper.getId());
    if (isExits) {
      changeIconFavorite(false);
      wallpaper.setFavorite(0);

    } else {
      changeIconFavorite(true);
      wallpaper.setFavorite(1);
    }
    db.updateWallpaper(wallpaper);
  }


  @Override
  public View contentView() {
    setWallpaperBinding = ActivitySetWallpaperBinding.inflate(getLayoutInflater());
    return setWallpaperBinding.getRoot();
  }

  private void getDataIntent() {
    Bundle bundle = getIntent().getExtras();
    if (bundle == null) {
      return;
    }
    wallpaper = (Wallpaper) bundle.getSerializable("wallpaper");
    if (wallpaper != null) {
      Picasso.with(getApplicationContext()).load(wallpaper.getPath())
              .placeholder(R.drawable.ic_no_thumbnail)
              .error(R.drawable.ic_no_thumbnail)
              .into(setWallpaperBinding.imgWallpaper);
      wallpaper.setView(wallpaper.getView() + 1);
      db.updateWallpaper(wallpaper);
    }

  }

  private void setWallpaper(String type) {
    ProgressDialog dialog = new ProgressDialog(this);
    dialog.setMessage(getString(R.string.please_set_wallpaper));
    dialog.show();
    Picasso.with(this)
            .load(wallpaper.getPath())
            .into(new Target() {
              @Override
              public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from){
                if (type.equalsIgnoreCase("Home")) {
                  homeWall(bitmap);
                } else {
                  lockWall(bitmap);
                }
                isMenuDisable();
                dialog.dismiss();
              }

              @Override
              public void onBitmapFailed(Drawable errorDrawable) {
                dialog.dismiss();
              }

              @Override
              public void onPrepareLoad(Drawable placeHolderDrawable) {

              }
            });
  }

  private void isMenuDisable() {
    if (!isAllVisibleFab) {
      setWallpaperBinding.fab.menu.setImageResource(R.drawable.ic_close);
      setWallpaperBinding.fab.homeWall.show();
      setWallpaperBinding.fab.lockWall.show();
      setWallpaperBinding.fab.favorite.show();
      setWallpaperBinding.fab.download.show();
      setWallpaperBinding.fab.bottom.show();
      isAllVisibleFab = true;
    } else {
      setWallpaperBinding.fab.menu.setImageResource(R.drawable.ic_menu);
      setWallpaperBinding.fab.homeWall.hide();
      setWallpaperBinding.fab.lockWall.hide();
      setWallpaperBinding.fab.favorite.hide();
      setWallpaperBinding.fab.download.hide();
      setWallpaperBinding.fab.bottom.hide();
      isAllVisibleFab = false;
    }
  }

//  public void rightTooltip(View view) {
//    Tooltip.make(this,new Tooltip.Builder()
//            .anchor(button2, Tooltip.Gravity.RIGHT)
//            .closePolicy(new Tooltip.ClosePolicy()
//                    .insidePolicy(true,false)
//                    .outsidePolicy(true,false),4000)
//            .activateDelay(900)
//            .showDelay(400)
//            .text("Android tooltip right")
//            .maxWidth(600)
//            .withArrow(true)
//            .withOverlay(true)
//            .build())
//            .show();
//  }

  @Override
  public void onBackPressed() {
    EventBus.getDefault().postSticky(new MessageEvent("UPDATE"));
    isADSFull();
    finish();
    super.onBackPressed();
  }

}
