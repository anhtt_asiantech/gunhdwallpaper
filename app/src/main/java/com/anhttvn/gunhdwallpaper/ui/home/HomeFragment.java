package com.anhttvn.gunhdwallpaper.ui.home;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.gunhdwallpaper.R;
import com.anhttvn.gunhdwallpaper.adapter.PhotoAdapter;
import com.anhttvn.gunhdwallpaper.databinding.FragmentHomeBinding;
import com.anhttvn.gunhdwallpaper.layout.SetWallpaper;
import com.anhttvn.gunhdwallpaper.model.Wallpaper;
import com.anhttvn.gunhdwallpaper.util.BaseFragment;
import com.anhttvn.gunhdwallpaper.util.Config;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
/**
 * @author anhtt61
 * @version 1.1.1
 * @2022
 */

public class HomeFragment extends BaseFragment implements PhotoAdapter.OnclickImage{

  private FragmentHomeBinding homeBinding;
  private List<Wallpaper> listImage = new ArrayList<>();
  private DatabaseReference mDatabase;

  private PhotoAdapter mPhotoAdapter;
  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    homeBinding = FragmentHomeBinding.inflate(inflater, container, b);
    return homeBinding.getRoot();
  }

  @Override
  protected void init() {
    mDatabase = FirebaseDatabase.getInstance().getReference();
    homeBinding.progress.setVisibility(View.VISIBLE);
    homeBinding.wallpapers.setVisibility(View.GONE);
    homeBinding.noData.getRoot().setVisibility(View.GONE);
    showAdsBanner(homeBinding.homeAds);
    if (isConnected()) {
      loadData();
    } else {
      listImage = db.listWallpaper();
      if (listImage.size() > 0) {
        adapter(listImage);
      }
    }

  }

  private void adapter(List<Wallpaper> wallpapers) {
    listImage = wallpapers;
    if (listImage != null && listImage.size() > 0) {
      homeBinding.wallpapers.setVisibility(View.VISIBLE);
      homeBinding.noData.getRoot().setVisibility(View.GONE);
      mPhotoAdapter = new PhotoAdapter(getActivity(), listImage, isConnected(), this);
      RecyclerView.LayoutManager layoutManager =
              new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
      homeBinding.wallpapers.setLayoutManager(layoutManager);
      homeBinding.wallpapers.setItemAnimator(new DefaultItemAnimator());
      homeBinding.wallpapers.setAdapter(mPhotoAdapter);
      mPhotoAdapter.notifyDataSetChanged();
    } else {
      homeBinding.wallpapers.setVisibility(View.GONE);
      homeBinding.noData.getRoot().setVisibility(View.VISIBLE);
    }
    homeBinding.progress.setVisibility(View.GONE);
  }

  private List<Wallpaper> loadData()  {
    List<Wallpaper> list = new ArrayList<>();
    DatabaseReference ref2 =  mDatabase.child(Config.WALLPAPER);
    ref2.addListenerForSingleValueEvent(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        for (DataSnapshot dsp : dataSnapshot.getChildren()) {
          Wallpaper wallpaper = dsp.getValue(Wallpaper.class);
          wallpaper.setView(db.countView(wallpaper.getId()));
          wallpaper.setFavorite(db.isFavoriteWallpaper(wallpaper.getId()) ? 1: 0);
          if (!db.isWallpaper(wallpaper.getId())) {
            db.addWallpaper(wallpaper);
          }
          list.add(wallpaper);
        }
        adapter(list);

      }

      @Override
      public void onCancelled(@NonNull DatabaseError error) {
        listImage = db.listWallpaper();
        if (listImage.size() > 0) {
          adapter(listImage);
        }
      }
    });

    return list;
  }


  @Override
  public void selectedPosition(int position) {
    if (!isConnected()) {
      Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
      return;
    }
    Intent intent = new Intent(getActivity(), SetWallpaper.class);
    intent.putExtra("wallpaper", listImage.get(position));
    startActivity(intent);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Config.REQUEST_CODE_REFRESH) {
      listImage = db.listWallpaper();
      adapter(listImage);
    }

  }



}