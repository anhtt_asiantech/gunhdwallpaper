package com.anhttvn.gunhdwallpaper.ui.download;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anhttvn.gunhdwallpaper.adapter.GalleryAdapter;
import com.anhttvn.gunhdwallpaper.databinding.FragmentFavoriteBinding;
import com.anhttvn.gunhdwallpaper.layout.SetWallpaper;
import com.anhttvn.gunhdwallpaper.layout.SettingWallpaper;
import com.anhttvn.gunhdwallpaper.util.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class DownloadFragment extends BaseFragment implements GalleryAdapter.EventGallery {
  private FragmentFavoriteBinding downloadBinding;
  private List<String> images = new ArrayList<>();
  private GalleryAdapter adapter;
  @Override
  protected View initView(LayoutInflater inflater, ViewGroup container, boolean b) {
    downloadBinding = FragmentFavoriteBinding.inflate(getLayoutInflater());
    return downloadBinding.getRoot();
  }

  @Override
  protected void init() {
    downloadBinding.noData.getRoot().setVisibility(View.GONE);
    downloadBinding.progressBar.setVisibility(View.GONE);
    adapter();
  }

  private void adapter(){
    images = getAllFileFolder();
    if (images != null && images.size() > 0) {
      downloadBinding.listRecent.setVisibility(View.VISIBLE);
      downloadBinding.noData.getRoot().setVisibility(View.GONE);
      adapter = new GalleryAdapter(getActivity(), images, "folder", this);
      RecyclerView.LayoutManager layoutManager =
              new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);
      downloadBinding.listRecent.setLayoutManager(layoutManager);
      downloadBinding.listRecent.setItemAnimator(new DefaultItemAnimator());
      downloadBinding.listRecent.setAdapter(adapter);
      adapter.notifyDataSetChanged();
    } else {
      downloadBinding.listRecent.setVisibility(View.GONE);
      downloadBinding.noData.getRoot().setVisibility(View.VISIBLE);
    }
  }

  @Override
  public void sendWallpaper(int position) {
    Intent intent = new Intent(getActivity(), SettingWallpaper.class);
    intent.putExtra("wallpaper", images.get(position));
    intent.putExtra("type", "Folder");
    startActivity(intent);
  }
}
