package com.anhttvn.gunhdwallpaper.model;

import java.io.Serializable;

public class Wallpaper implements Serializable {
  public String id;
  public String path;
  public String title;
  public int view;

  public int getView() {
    return view;
  }

  public void setView(int view) {
    this.view = view;
  }

  public int getFavorite() {
    return favorite;
  }

  public void setFavorite(int favorite) {
    this.favorite = favorite;
  }

  public int favorite;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

}
