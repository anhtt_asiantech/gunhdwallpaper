package com.anhttvn.gunhdwallpaper.util;

import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.anhttvn.gunhdwallpaper.database.DatabaseHandler;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public abstract class BaseFragment extends Fragment {

  protected DatabaseHandler db;
  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    return initView(inflater,container,false);
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    db = new DatabaseHandler(getActivity());
    init();
  }

  protected abstract View initView(LayoutInflater inflater, ViewGroup container, boolean b);
  protected abstract void init();

  protected boolean isConnected() {
    return com.anhttvn.gunhdwallpaper.util.Connectivity.isConnectedFast(getActivity());
  }

  protected void showAdsBanner (AdView ads) {
    AdRequest adRequest = new AdRequest.Builder()
            .addTestDevice("2C995D2A909C1537C3C52A40B8DA69D9").build();
    if (isConnected()) {
      ads.setVisibility(View.VISIBLE);
      ads.loadAd(adRequest);
    }else{
      ads.setVisibility(View.GONE);
    }
  }

  private String getExternalStorageDirectory() {
    String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
    return baseDir != null && baseDir.length() > 0 ?  baseDir : null;
  }

  private File getExternalFilesDir() {
    File path = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
    return path.getAbsolutePath() != null ? path : null;
  }

  protected List<String> getAllFileFolder() {
    List<String> list = new ArrayList<>();
    String path =
            getExternalFilesDir() != null ? getExternalFilesDir() +"/" +File.separator + "/" + com.anhttvn.gunhdwallpaper.util.Config.FOLDER_DOWNLOAD :
                    getExternalStorageDirectory() +File.separator+ com.anhttvn.gunhdwallpaper.util.Config.FOLDER_DOWNLOAD;
    if (path == null) {
      return list;
    }
    File root =new File(path);
    File[] files= root.listFiles();
    list.clear();
    for (File file: files){
      list.add(file.getPath());
    }

    return list;
  }
}
