package com.anhttvn.gunhdwallpaper.util;

public class Config {
  public static final String DB_IMAGE = "wallpaper";
  public static final String FOLDER_DOWNLOAD ="GunHDWallpaper";
  public static  final String NEW_WALLPAPER ="NewWallpaper";
  public static final String WALLPAPER ="Wallpaper";

  public static final String URL_APP = "https://play.google.com/store/apps/details?id=com.anhttvn.gunhdwallpaper";
  public  static final int PERMISSION_REQUEST_CODE = 7;

  public static final int REQUEST_CODE_REFRESH = 10;

}
